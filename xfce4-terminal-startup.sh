home_dir=$HOME
# offlineimap_dir="$HOME"/git/offlineimap.py

# Wrapper for bash command
# Argument 1 is the command to be wrapped
# Argument 2 is the variable to which the command is prescribed to
# e.g. bash_cmd cmd var_map
# function bash_cmd() {
#   local  __resultvar=$2
#   local  myresult=$1
#   eval $__resultvar="'env PROMPT_COMMAND=\"unset PROMPT_COMMAND; $myresult\" bash'"
# }

xfce4-terminal -T "workbench" --working-directory="$home_dir" \
  --tab -T "scratch 1" --working-directory="$home_dir" \
  --tab -T "scratch 2" --working-directory="$home_dir" \
  --tab -T "yum" --working-directory="$home_dir" \
  --tab -T "ssh" --working-directory="$home_dir" \
  --tab -T "mutt" --working-directory="$home_dir" \
  --tab -T "offlineimap" --working-directory="$home_dir"
# --tab -T "offlineimap" --working-directory="$home_dir" -e "$offlineimap_dir/offlineimap.py"
