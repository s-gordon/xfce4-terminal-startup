# xfce4-terminal-startup

## Installation

Add the following keyboard shortcut:

```sh
<absolute/path/to/repository/folder>/xfce4-terminal-startup/xfce4-terminal-startup.sh
```

I map mine to Super-T.
